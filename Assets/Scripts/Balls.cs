using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balls : MonoBehaviour
{
    public int idBalls;
    public bool isCheck;

    public Material[] sprites;

    public void SetUpStart()
    {
        isCheck = false;
        idBalls = Random.Range(0, sprites.Length);
        Debug.Log("id = " + idBalls);
        GetComponent<SpriteRenderer>().material = sprites[idBalls];
    }
}
