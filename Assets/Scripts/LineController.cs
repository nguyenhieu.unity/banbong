using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour
{
    LineRenderer lineRender;

    public Transform gunTransform;

    Vector3 firstdirection;


    public Transform finalBall;


    public List<GameObject> listTestLocation;


    public Vector3 hitPos;
    RaycastHit2D point2;

    public GameObject BallShoot;

    public Vector3Int BallSelect = new Vector3Int();


    public Transform GridMapGame;

    void Start()
    {
        lineRender = GetComponent<LineRenderer>();
    }




    void Update()
    {

        if (Input.GetMouseButton(0))
        {

            lineRender.enabled = true;

            Vector3 posMouse = Input.mousePosition;

            posMouse.z = 0;

            Vector3 pos = Camera.main.ScreenToWorldPoint(posMouse);

            firstdirection = pos - gunTransform.position;

            var point1 = Physics2D.Raycast(gunTransform.position, firstdirection);

            if (point1.collider != null)
            {

                Debug.DrawLine(gunTransform.transform.position, point1.point, Color.green);

                if (point1.collider.CompareTag("Wall"))
                {
                    lineRender.positionCount = 3;

                    hitPos = point1.point;
                    Vector2 r = new Vector2(hitPos.x, hitPos.y);

                    Vector2 newDir = Vector2.Reflect(firstdirection, point1.normal).normalized;
                    Vector3 newr = Vector2.Reflect(firstdirection, point1.normal);


                    point2 = Physics2D.Raycast(r + newDir * 0.0001f, newDir);


                    lineRender.SetPosition(0, gunTransform.position);

                    lineRender.SetPosition(1, point1.point);

                    listTestLocation[0].transform.position = point1.point;


                    if (point2.collider != null)
                    {
                        //if(point2.collider.CompareTag("ball"))
                        {
                            finalBall.position = point2.point;

                            listTestLocation[1].transform.position = point2.point;
                            Debug.DrawLine(point1.point, point2.point, Color.green);
                            lineRender.SetPosition(2, point2.point);
                            if (point2.collider.CompareTag("ball"))
                            {
                                BallSelect.x = (int)point2.collider.gameObject.transform.position.x;
                                BallSelect.y = (int)point2.collider.gameObject.transform.position.y;
                            }
                            else
                            {
                                BallSelect = Vector3Int.zero;
                            }
                        }
                    }

                }
                else if (point1.collider.CompareTag("ball"))
                {


                    lineRender.positionCount = 2;
                    hitPos = point1.point;

                    listTestLocation[0].transform.position = hitPos;
                    lineRender.SetPosition(0, gunTransform.position);
                    lineRender.SetPosition(1, point1.point);

                    finalBall.position = point1.point;


                    BallSelect.x = (int)point1.collider.gameObject.transform.position.x;
                    BallSelect.y = (int)point1.collider.gameObject.transform.position.y;

                }

                else
                {
                    lineRender.positionCount = 2;
                    hitPos = point1.point;

                    listTestLocation[0].transform.position = hitPos;
                    lineRender.SetPosition(0, gunTransform.position);
                    lineRender.SetPosition(1, point1.point);
                    finalBall.position = point1.point;
                    BallSelect = Vector3Int.zero;


                }
            }

        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (Mathf.Abs(BallSelect.x + BallSelect.y) % 2 == 1)
            {

                GameObject ball = Instantiate(BallPrefabs, InsPosition(BallSelect, finalBall.position), Quaternion.identity);
                ball.GetComponent<Balls>().SetUpStart();
                ball.name = "[" + InsPosition(BallSelect, finalBall.position).x + "," + InsPosition(BallSelect, finalBall.position).y + "]";
                ball.transform.SetParent(GridMapGame);


                // lay tat ca cac qua bong dang co tren map 

                ListBalls();

                // add vao queue de quy viec tim qua bong can no

                queueBallsSelect = new Queue<Balls>();
                queueBallsSelect.Enqueue(ball.GetComponent<Balls>());

                Debug.Log(queueBallsSelect.Count);

                GetSameBalls();
            }



        }

        else if (Input.GetMouseButtonDown(0))
        {

        }

        if (Input.GetMouseButtonDown(1))
        {
            //ListBalls();
            //Debug.Log(" get balls ");
        }
    }
    public GameObject BallPrefabs;
    public Vector3Int InsPosition(Vector3Int otherCollision, Vector3 final)
    {
        Vector3Int posIns = new Vector3Int();
        posIns.y = otherCollision.y - 1;
        if (final.x < otherCollision.x)
        {
            posIns.x = otherCollision.x - 1;
        }
        else
        {
            posIns.x = otherCollision.x + 1;
        }


        return posIns;
    }




    [Space(5)]
    [Header(" Same Balls ")]
    [Space(5)]
    [SerializeField] List<Vector3Int> NeighborBalls = new List<Vector3Int>();

    public List<Balls> listBalls;


    public Queue<Balls> queueBallsSelect;


    public void ListBalls()
    {
        listBalls = new List<Balls>();
        Balls[] balls = FindObjectsOfType<Balls>();
        for (int i = 0; i < balls.Length; i++)
        {
            listBalls.Add(balls[i]);
            balls[i].GetComponent<Balls>().isCheck = false;
        }

        Debug.Log(listBalls.Count);

    }
    public Balls TestAddQueue;

    [ContextMenu("Get Same Ball")]
    public void GetSameBalls()
    {
        ballsdelete.Clear();
        int i = 1;
        while (queueBallsSelect.Count > 0)
        {
            Debug.Log(i);
            i++;
            var item = queueBallsSelect.Dequeue();

            ballsdelete.Add(item);
            item.isCheck = true;
            Neighbor(Vector3Int.RoundToInt(item.transform.position), item.idBalls);
        }
        Debug.Log(" i= " + i);
        if (i > 3)
        {
            DeleteBalls();
        }
    }



    public void Neighbor(Vector3Int myInt, int id)
    {
        List<Vector3Int> nei = new List<Vector3Int>();

        nei.Add(myInt + new Vector3Int(1, 1, 0));
        nei.Add(myInt + new Vector3Int(1, -1, 0));
        nei.Add(myInt + new Vector3Int(-1, 1, 0));
        nei.Add(myInt + new Vector3Int(-1, -1, 0));

        foreach (var item in listBalls)
        {
            for (int i = 0; i < nei.Count; i++)
            {
                if (nei[i] == item.transform.position)
                {
                    if (item.idBalls == id && item.isCheck == false)
                    {
                        queueBallsSelect.Enqueue(item);
                        // Check bong da duoc check roi thi bo qua
                        item.isCheck = true;
                    }
                }
            }
        }
    }


    public List<Balls> ballsdelete = new List<Balls>();

    public void DeleteBalls()
    {
        foreach (var item in listBalls)
        {
            if (item.isCheck == true)
            {
                //Destroy(item.gameObject);
                item.gameObject.SetActive(false);
            }
        }

    }
}
