using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GridMapGame : MonoBehaviour
{
    public GameObject gameTile;

    public int width, height;
    private void InitMap()
    {
        Camera.main.transform.position = new Vector3(width / 2 - (float)((width + 1) % 2) / 2, height / 2 - 5, -10);


        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if ((i + j) % 2 == 0) continue;
                GameObject ob = Instantiate(gameTile);
                ob.GetComponent<Balls>().SetUpStart();

                ob.transform.position = new Vector3(i, j , 0);
                ob.name = "[" + i + "," + j + "]";
                ob.transform.SetParent(transform);

            }
        }
    }

    private void Start()
    {
        InitMap();
    }
}
